from sys import argv
import string

def getNumberOfProblems(inputFile):
	return int(inputFile.readline())

def getLetters(line):
	tempLine = line.upper()
	result = {}
	for letter in tempLine:
		if letter in string.uppercase:
			if result.has_key(letter):
				result[letter] = result[letter] + 1
			else:
				result[letter] = 1
	return result

def getMaximumValue(letterList):
	maxValue = 0
	actualValue = 26
	values = letterList.values()
	values.sort(reverse = True)
	for letter in values:
		maxValue += letter * actualValue
		actualValue = actualValue - 1

	return maxValue

if __name__ == '__main__':
	filePath = argv[1]
	inputFile = open(filePath, 'r')
	outputFile = open('./output', 'w')
	n = getNumberOfProblems(inputFile)
	for problem in range(n):
		line = inputFile.readline()
		letterList = getLetters(line)
		#sortList(letterList)
		maxValue = getMaximumValue(letterList)
		outputFile.write("case #" + str(problem+1) + ": " + str(maxValue)+'\n')
		#print("case #" + str(problem+1) + ": " + str(maxValue))